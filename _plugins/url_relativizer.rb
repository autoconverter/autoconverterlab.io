# from https://github.com/jekyll/jekyll/issues/6360#issuecomment-329275101
# & https://github.com/jekyll/jekyll/pull/6362/files#diff-ebe897bfe8557ae1df5e2254e834bf29a7d3f737af1dc1c5d089f8779ec0e8b4R36
require 'pathname'

module Jekyll
  module UrlRelativizer
    # Produces a path relative to the current page. For example, if the path
    # `/images/me.jpg` is supplied, then the result could be, `me.jpg`,
    # `images/me.jpg` or `../../images/me.jpg` depending on whether the current
    # page lives in `/images`, `/` or `/sub/dir/` respectively.
    #
    # input - a path relative to the project root
    #
    # Returns the supplied path relativized to the current page.
    def relativize_url(input)
    return if input.nil?
    input = ensure_leading_slash(input)
    page_url = @context.registers[:page]["url"]
    page_dir = Pathname(page_url).parent
    Pathname(input).relative_path_from(page_dir).to_s
    end
  end
end

Liquid::Template.register_filter(Jekyll::UrlRelativizer)
