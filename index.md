---
layout: default
title: AutoConverter — Ein modellbasierter, intelligenter (Geo-)Datenkonverter für heterogene Systeme
---

## AutoConverter — Ein modellbasierter, intelligenter (Geo-)Datenkonverter für heterogene Systeme

Das innovative Projekt und die Software 'AutoConverter' bezwecken
eine massive (überproportionale) Reduktion
des manuellen Aufwands zur Datenschema-Konvertierung
beim Datenaustausch zwischen föderierten Systemen.
Föderierte Systeme sind verbreitet und haben sich bewährt.
Man findet sie beispielsweise
im Bauwesen im Building Information Modeling (BIM) und
bei raumbezogenen Daten (Geodaten), wo
sowohl private (z.B. Ingenieurbüros)
als auch öffentliche Akteure (Behörden)
beteiligt sind.


Um den manuellen Aufwand zur Informations-Integration
bzw. zur Datenschema-Konvertierung zu reduzieren,
soll neu die Konversionslogik automatisch ermittelt werden.
Die generierten Datenverarbeitungs-Instruktionen werden dann
für die eigentliche Schema-Transformation und Daten-Konversion angewandt.
Eine bereits weitgehend bestehende Software 'ConvConf'
ermittelt die Konversionslogik
und zwar aus maschinenlesbaren Beschreibungen
eines gemeinsamen konzeptionellen Modells (KM)
– beschrieben in INTERLIS (Norm SN 612031) –
sowie aus den Ein- und Ausgangsschematas und
deren jeweiligen Beziehungen zum KM.


Im nächsten Realisierungsschritt soll nun
ein Konverter entwickelt und getestet werden,
der die generierten Datenverarbeitungs-Instruktionen auf Geodaten anwendet und
diese so transformiert.


Möchten Sie mehr über dieses Projekt erfahren?
Kontaktieren Sie Prof. Stefan Keller (<stefan.keller@ost.ch>)
oder konsultieren Sie das [Product-Factsheet zum AutoConverter][factsheet].

[factsheet]: {{ "/assets/Product_Factsheet_AutoConverter_2022_v3.pdf" | relativize_url }}
